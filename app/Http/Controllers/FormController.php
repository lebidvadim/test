<?php

namespace App\Http\Controllers;

use App\Models\Car;
use App\Models\CarsModel;
use Illuminate\Http\Request;

class FormController extends Controller
{
    public function index()
    {
        $cars = Car::all()->toArray();

        return view('form', ['cars' => $cars]);
    }
    public function getModel(Request $request) {
        $id = $request->post('id');
        $model = CarsModel::where(['cars_id' => $id])->get();
        return $model;
    }
    public function send(Request $request){
        $carId = $request->post('id_car');
        $modelId = $request->post('id_model');

        if($modelId and $carId){
            $car = Car::find($carId)->toArray();
            $model = CarsModel::find($modelId)->toArray();
            return json_encode(['car' => $car,'model' => $model]);
        }
    }
}
