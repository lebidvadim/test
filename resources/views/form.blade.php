<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Form</title>
</head>
<body>
    <div class="container pt-5 pb-5">
        <div class="row">
            <div class="col-12 col-md-6">
                <select class="form-select" id="cars" onchange="">
                    <option value="0" selected>Виберіть марку авто</option>
                    @foreach($cars as $car)
                    <option value="{{ $car['id'] }}">{{ $car['name'] }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-12 col-md-6">
                <select class="form-select" id="model" disabled>
                    <option value="0" selected>Виберіть модель авто</option>
                </select>
            </div>
            <div class="col mt-3">
                <button class="btn btn-primary d-none" id="submit">Відправити</button>
            </div>
            <div class="col mt-3 d-none" id="result">
                <div class="row">
                    <div class="col" id="brand_name"><strong>Марка: </strong><span></span></div>
                    <div class="col" id="model_name"><strong>Модель: </strong><span></span></div>
                </div>
            </div>
        </div>

    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.3.js"></script>
    <script type="application/javascript">
        var id_car = null;
        var id_model = null;
        $('#cars').on('change',function (){
            $('#result').addClass('d-none');
            id_car = this.value;
            $.ajax({
              method: "POST",
              url: '/',
              data: { id: this.value }
            }).done(function( model ) {
                $('#model').html('<option value="0" selected>Виберіть модель авто</option>');
                if(model.length > 0){
                    for(let i = 0; i < model.length; i++){
                        $('#model').removeAttr('disabled').append('<option value="'+model[i].id+'">'+model[i].name_model+'</option>');
                    }
                }
                else{
                    $('#result').addClass('d-none')
                    $('#submit').addClass('d-none');
                    $('#model').attr('disabled','disabled');
                }
            });
        });
        $('#model').on('change',function (){
            id_model = this.value;
            $('#submit').removeClass('d-none');
        });

        $('#submit').on('click',function (){
            $.ajax({
              method: "POST",
              url: '/send',
              data: { id_car: id_car, id_model: id_model}
            }).done(function( result ) {
                result = $.parseJSON(result);
                if(result){
                    $('#result').removeClass('d-none');
                    $('#result #brand_name > span').text(result.car.name);
                    $('#result #model_name > span').text(result.model.name_model);
                }
            });
        });

    </script>
</body>
</html>