<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class CarsModel extends Model
{
    use HasFactory;
    protected $table = 'cars_model';

    protected $fillable = ['cars_id','name_model'];

    public function car() : HasOne {
        return $this->hasOne(Car::class);
    }
}
