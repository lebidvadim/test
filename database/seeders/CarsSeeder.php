<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Psy\Util\Str;

class CarsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('cars')->insertOrIgnore([
            ['name' => 'Audi'],
            ['name' => 'Mazda'],
            ['name' => 'Opel'],
        ]);

        DB::table('cars_model')->insertOrIgnore([
            ['car_id' => 1, 'name_model' => 'model 1'],
            ['car_id' => 1, 'name_model' => 'model 2'],
            ['car_id' => 2, 'name_model' => 'model 1'],
            ['car_id' => 2, 'name_model' => 'model 2'],
            ['car_id' => 3, 'name_model' => 'model 1'],
            ['car_id' => 3, 'name_model' => 'model 2'],
        ]);
    }
}
